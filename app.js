//app.js
App({
  onLaunch: function() {
    // 当小程序初始化完成时，会触发 onLaunch（全局只触发一次）
    // 展示本地存储能力,调用API从本地缓存中获取数据
    var logs = wx.getStorageSync('logs') || []
    logs.unshift(Date.now())
    wx.setStorageSync('logs:', logs)

    setInterval(function() {
      wx.getNetworkType({
        success(res) {
          console.info('网络检测结果res:' + JSON.stringify(res))
          // 返回网络类型, 有效值：
          // wifi/2g/3g/4g/unknown(Android下不常见的网络类型)/none(无网络)
          const networkType = res.networkType
          console.log('网络类型:' + networkType)
          if (networkType == 'none') {
            wx.showToast({
              title: '网络异常，请检查网络是否已连接',
              icon: 'none',
              duration: 3000
            })
            return
          }
        }
      })
      // 监听网络状态变化事件
      wx.onNetworkStatusChange(function(res) {
        console.log('网络连接状态:' + res.isConnected)
        console.log('网络类型:' + res.networkType)
      })
    }, 10000)


    // 登录
    wx.login({
      success: res => {
        // 发送 res.code 到后台换取 openId, sessionKey, unionId
      }
    })
    // 获取用户信息
    wx.getSetting({

      success: res => {
        if (res.authSetting['scope.userInfo']) {
          // 已经授权，可以直接调用 getUserInfo 获取头像昵称，不会弹框
          wx.getUserInfo({
            success: res => {
              // 可以将 res 发送给后台解码出 unionId
              this.globalData.userInfo = res.userInfo

              // 由于 getUserInfo 是网络请求，可能会在 Page.onLoad 之后才返回
              // 所以此处加入 callback 以防止这种情况
              if (this.userInfoReadyCallback) {
                this.userInfoReadyCallback(res)
              }
            }
          })
        }
      }
    })
  },
  onShow: function() {
    // 当小程序启动，或从后台进入前台显示，会触发 onShow

  },

  onHide: function() {
    // 当小程序从前台进入后台，会触发 onHide
  },
  globalData: {
    userInfo: null
  }
})