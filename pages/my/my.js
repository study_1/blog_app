// pages/my/my.js
Page({
  /**
   * 页面的初始数据
   */
  data: {
    title: 'Tab名称',
    img: 'https://timgsa.baidu.com/timg?image&quality=80&size=b9999_10000&sec=1556068672&di=ea0108d400c429029ee303f8bf484fb0&imgtype=jpg&er=1&src=http%3A%2F%2Fwww.zglb17.com%2FImg%2Fpage1.jpg',
    result: '',
    charSet: '',
    scanType: '',
    path: '',
    canIUse: wx.canIUse('cover-view'),
    routers: [{
        name: 'HTML',
        url: '/pages/news/news',
        icon: '../resource/images/my.png',
        code: '10'
      },
      {
        name: 'Java',
        url: '/pages/news/news',
        icon: '../resource/images/my.png',
        code: '11'
      },
      {
        name: 'CSS',
        url: '/pages/news/news',
        icon: '../resource/images/my.png',
        code: '10'
      },
      {
        name: 'PHP',
        url: '/pages/news/news',
        icon: '../resource/images/my.png',
        code: '11'
      },
      {
        name: 'Python',
        url: '/pages/news/news',
        icon: '../resource/images/my.png',
        code: '10'
      },
      {
        name: 'JavaScript',
        url: '/pages/news/news',
        icon: '../resource/images/my.png',
        code: '11'
      },
      {
        name: 'C++',
        url: '/pages/news/news',
        icon: '../resource/images/my.png',
        code: '10'
      },
      {
        name: 'Object-C',
        url: '/pages/news/news',
        icon: '../resource/images/my.png',
        code: '11'
      },
      {
        name: 'Ruby',
        url: '/pages/news/news',
        icon: '../resource/images/my.png',
        code: '10'
      }
    ]
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function(options) {
    var that = this
    wx.setNavigationBarTitle({
      title: 'Tab选项',
    })
  },
  getScancode: function() {
    var _this = this;
    // 允许从相机和相册扫码
    wx.scanCode({
      success: (res) => {
        console.log(res)
        _this.setData({
          result: res.result,
          scanType: res.scanType,
          charSet: res.charSet,
          path: res.path
        })
        if (res.errMsg == 'scanCode:ok') {
          wx.showToast({
            title: '扫码成功',
            icon: 'success',
            duration: 2000
          })
        }
        //扫码成功跳转带返回按钮的页面
        wx.navigateTo({
          url: '../scanCode/scancode',
        })
      },
      fail: (res) => {
        console.error(res)
        if (res.errMsg == 'scanCode:fail cancel') {
          wx.showToast({
            title: '取消扫码',
            icon: 'none',
            duration: 2000
          })
        } else {
          wx.showToast({
            title: '扫码异常',
            icon: 'none',
            duration: 2000
          })
        }
      },
      complete: (res) => {
        console.log('不管成功还是失败，此方法都执行!')
      }
    })
    // 只允许从相机扫码
    // wx.scanCode({
    //   onlyFromCamera: true,
    //   success: (res) => {
    //     console.log(res)
    //   }
    // })
  },

  printText(){
    wx.chooseImage({
      count:1,
      sizeType: ['original', 'compressed'],
      sourceType: ['album', 'camera'],
      complete: (res) => {
        const tempFilePaths=res.tempFilePaths
        var filePath=encodeURI(tempFilePaths)
        let buffer=wx.getFileSystemManager().readFileSync(filePath)
        console.log(filePath)
        wx.cloud.callFunction({
          name:'upload',
          data:{
            //imgUrl:encodeURI("https://www.toutiao.com/123.jpg"),
            file:buffer
          },
          complete:console.log
        })
      },
    })
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function() {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function() {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function() {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function() {

  }
})