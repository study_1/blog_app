// pages/news.js
Page({

  /**
   * 页面的初始数据
   */
  data: {
    result: false,
    imgUrls: [
      '../resource/images/login_bg_02.jpg',
      '../resource/images/login_bg_03.jpg',
      '../resource/images/login_bg_02.jpg'
    ],
    img: 'https://timgsa.baidu.com/timg?image&quality=80&size=b9999_10000&sec=1555473849858&di=19848f1ff76917701d8556ba5d0bff63&imgtype=0&src=http%3A%2F%2Fs14.sinaimg.cn%2Fmw690%2F007nKqxnzy7sCdH9RzDbd%26690',
    createTime: null,
    title: null,
    content: null,
    viewCount: null
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function(options) {
    var that = this;
    let data = wx.getStorageSync("blog")
    if (data == "") {
      console.log('缓存为空')
      this.setData({
        result: true
      })
    } else {
      console.log('获取到缓存数据:', data)
      let json = JSON.parse(data)
      that.setData({
        title: json.title,
        content: json.content,
        createTime: timestampToTime(json.createTime),
        viewCount: json.viewCount
      })
    }

    function timestampToTime(timestamp) {
      var date = new Date(timestamp); //时间戳为10位需*1000，时间戳为13位的话不需乘1000
      var Y = date.getFullYear() + '-';
      var M = (date.getMonth() + 1 < 10 ? '0' + (date.getMonth() + 1) : date.getMonth() + 1) + '-';
      var D = date.getDate() + ' ';
      var h = date.getHours() + ':';
      var m = date.getMinutes() + ':';
      var s = date.getSeconds();
      return Y + M + D + h + m + s;
    }
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function() {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function() {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function() {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function() {

  }
})