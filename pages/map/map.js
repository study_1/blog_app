Page({
  data: {
    latitude: '',
    longitude: '',
    scale: 16,
    location: "获取位置",
    markers: [{
      id: 1,
      latitude: 23.099994,
      longitude: 113.32452,
      title: '我的位置',
    }, {
      id: 2,
      latitude: 23.099994,
      longitude: 113.32452,
      iconPath: '../resource/images/location.png',
      callout: {
        content: '自定义点',
        color: '#AD1212',
        bgColor: '#00AD00',
        fontSize: '20',
        borderRadius: '5'
      }
    }],
  },
  onLoad: function(options) {
    var that = this
    wx.getLocation({
      type: 'gcj02', // 返回可以用于wx.openLocation的经纬度
      success(res) {
        const latitude = res.latitude
        const longitude = res.longitude
        wx.openLocation({
          latitude,
          longitude,
          scale: 18
        })
        that.setData({
          location: '经度:' + res.longitude + '纬度:' + res.latitude,
          latitude: res.latitude,
          longitude: res.longitude
        })
      }
    })
  },
  //初始化地图
  onReady: function(e) {
    this.mapCtx = wx.createMapContext('myMap')
  },
  //获取位置
  getCenterLocation: function() {
    var that = this
    that.mapCtx.getCenterLocation({
      success: function(res) {
        console.log('经度', res.longitude)
        console.log('纬度', res.latitude)
        that.setData({
          location: '经度:' + res.longitude + '纬度:' + res.latitude,
          latitude: res.latitude,
          longitude: res.longitude
        })
      }
    })

  },
  scaleClick: function() {
    this.setData({
      scale: 10,
    })
  },
  // 移动位置
  moveToLocation: function() {
    this.mapCtx.moveToLocation()
  },
  // 移动标注
  translateMarker: function() {
    this.mapCtx.translateMarker({
      markerId: 1,
      autoRotate: true,
      duration: 1000,
      destination: {
        latitude: 23.099994,
        longitude: 113.32452,
      },
      animationEnd() {
        console.log('动画结束')
      }
    })
  }
})