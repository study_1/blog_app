//logs.js
const util = require('../../utils/util.js')

Page({
  data: {
    logs: [],
    winWidth: 0,
    winHeight: 0,
    currentTab: 0,
    image: 'https://timgsa.baidu.com/timg?image&quality=80&size=b9999_10000&sec=1555473849858&di=19848f1ff76917701d8556ba5d0bff63&imgtype=0&src=http%3A%2F%2Fs14.sinaimg.cn%2Fmw690%2F007nKqxnzy7sCdH9RzDbd%26690',
    moreImage: 'https://timgsa.baidu.com/timg?image&quality=80&size=b9999_10000&sec=1556068089&di=1ec3d411931f7ca39e2e9d60e0c2243c&imgtype=jpg&er=1&src=http%3A%2F%2F5b0988e595225.cdn.sohucs.com%2Fimages%2F20180507%2F2bc2093573e5440ca19cf0e29a42378a.jpeg',
    logImage: 'https://timgsa.baidu.com/timg?image&quality=80&size=b9999_10000&sec=1556068310&di=08e2d2d88edbe1a1f0188311cf2e05e6&imgtype=jpg&er=1&src=http%3A%2F%2Fimg.zcool.cn%2Fcommunity%2F01f1ce5a1525b5a80120518720d7de.jpg%401280w_1l_2o_100sh.jpg'
  },
  onLoad: function() {
    this.setData({
      logs: (wx.getStorageSync('logs') || []).map(log => {
        return util.formatTime(new Date(log))
      })
    })
  },
  onReady: function() {
    var that = this;
    wx.getSystemInfo({
      success: function(res) {
        that.setData({
          winWidth: res.windowWidth,
          winHeight: res.windowHeight
        });
      }
    });
  },
  // 滑动切换tab
  bindChange: function(e) {
    var that = this;
    that.setData({
      currentTab: e.detail.current
    });
  },
  // 点击tab切换
  swichNav: function(e) {
    var that = this;
    if (this.data.currentTab === e.target.dataset.current) {
      return false;
    } else {
      that.setData({
        currentTab: e.target.dataset.current
      })
    }
  },

  onShow: function() {

  },
  onHide: function() {

  },
  onUnload: function() {

  },
})