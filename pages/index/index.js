//index.js
//获取应用实例
const app = getApp()

Page({
  data: {
    motto: 'index page',
    userInfo: {},
    hasUserInfo: false,
    canIUse: wx.canIUse('button.open-type.getUserInfo'),
    img: 'https://timgsa.baidu.com/timg?image&quality=80&size=b9999_10000&sec=1556068089&di=1ec3d411931f7ca39e2e9d60e0c2243c&imgtype=jpg&er=1&src=http%3A%2F%2F5b0988e595225.cdn.sohucs.com%2Fimages%2F20180507%2F2bc2093573e5440ca19cf0e29a42378a.jpeg'
  },
  //事件处理函数
  bindViewTap: function() {
    wx.navigateTo({
      url: '../logs/logs'
    })
  },
  onLoad: function() {
    // 获取系统信息存入缓存
    try {
      const sysInfo = wx.getSystemInfoSync()
      wx.setStorageSync("sysInfo", sysInfo)
      console.log('获取系统信息sysInfo:', sysInfo)
    } catch (e) {
      console.warn('获取系统信息失败!')
    }

    if (app.globalData.userInfo) {
      this.setData({
        userInfo: app.globalData.userInfo,
        hasUserInfo: true
      })
    } else if (this.data.canIUse) {
      // 由于 getUserInfo 是网络请求，可能会在 Page.onLoad 之后才返回
      // 所以此处加入 callback 以防止这种情况
      app.userInfoReadyCallback = res => {
        this.setData({
          userInfo: res.userInfo,
          hasUserInfo: true
        })
      }
    } else {
      // 在没有 open-type=getUserInfo 版本的兼容处理
      wx.getUserInfo({
        success: res => {
          app.globalData.userInfo = res.userInfo
          this.setData({
            userInfo: res.userInfo,
            hasUserInfo: true
          })
        }
      })
    }
  },
  getUserInfo: function(e) {
    // 获取用户信息存入缓存
    console.log('获取用户信息:' + JSON.stringify(e))
    wx.setStorageSync("userInfo", e.detail.userInfo)
    app.globalData.userInfo = e.detail.userInfo
    this.setData({
      userInfo: e.detail.userInfo,
      hasUserInfo: true
    })
  },
  getMenu: function() {
    wx.navigateTo({
      url: '../login/dxLogin/login',
    })
  },
  getNews: function() {
    wx.navigateTo({
      url: '../news/news',
    })
  },
  getScancode: function() {
    var _this = this;
    // 允许从相机和相册扫码
    wx.scanCode({
      success: (res) => {
        var result = res.result;
        var scanType = res.scanType;
        var charSet = res.charSet;
        var path = res.path;
        _this.setData({
          result: result,
          scanType: scanType,
          charSet: charSet,
          path: path
        })
        wx.showToast({
          title: '扫码成功',
          icon: 'success',
          duration: 2000
        })
        wx.setStorageSync("scan", res)
        console.log('扫码成功，跳转页面')
        wx.navigateTo({
          url: '../my/detail/scaninfo',
        })
      },
      fail: (res) => {
        console.error(res.errMsg)
        if (res.errMsg == 'scanCode:fail cancel') {
          wx.showToast({
            title: '取消扫码',
            icon: 'none', //不带icon
            duration: 2000
          })
        } else {
          wx.showToast({
            title: '扫码异常',
            icon: 'none',
            duration: 2000
          })
        }
      },
      complete: (res) => {
        console.log('扫码完成')
        //完成后，隐藏加载框
        //wx.hideLoading()
      }
    })
  },
  getMap: function() {
    wx.navigateTo({
      url: '../map/map',
    })
  },
  getHistory: function() {
    wx.navigateTo({
      url: '../wxuser/wxuser',
    })
  },
  getQrcode: function() {
    wx.showModal({
      title: '温馨提示',
      content: '二维码生成中,敬请期待',
      showCancel: false, //默认为true,false去掉取消按钮
      confirmText: '我知道了',//默认是“确认”
    	confirmColor: '',//确认文字的颜色
      cancelColor: 'cancelColor',
      success:function(res){
		  if(res.cancel){
			  //点击取消按钮
		  }else if(res.confirm){
			  //点击确认按钮
		  }
	}
    })
    // .showToast({
    //   title: '此功能尚未开发',
    //   icon: 'none',
    //   duration: 2000
    // })

  },
  getFind: function() {
      wx.navigateTo({
        url: '../search/search',
      })
  },
  getHelp: function() {
    wx.showToast({
      title: '正在完善中',
      icon: 'none',
      duration: 2000
    })
  },
  suggestion: function() {
    wx.navigateTo({
      url: '../suggestion/suggestion',
    })
  },
  takePhoto() {
    const ctx = wx.createCameraContext()
    ctx.takePhoto({
      quality: 'high',
      success: (res) => {
        this.setData({
          src: res.tempImagePath
        })
      }
    })
  },
  error(e) {
    console.log(e.detail)
  },
  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function() {
    return {
      title: '微信小程序',
      desc: '微信小程序学习，原生实现',
      path: '/pages/index/index'
    }
  },
  onShareTimeline:function(){
    var shareimg =[
      "../resource/images/login_bg_02.jpg",
      "../resource/images/r.jpg",
      "../resource/images/login_bg_03.jpg"
    ]
    var randomImg = shareimg[Math.floor(Math.random() * shareimg.length)];
    return {
      title: 'this is fine',
      query: 'test',
      imageUrl: randomImg, //此处就是写的随机分享图片,
    }
  }
})