const app = getApp()
const urlList = require('../resource/js/config.js');
Page({
  data: {},
  onLoad: function(params) {
    try {
      const sysInfo = wx.getSystemInfoSync()
      console.log('sysInfo:', sysInfo)
    } catch (e) {
      console.warn('获取系统信息失败!')
    }
  },
  // 登录  
  doLogin: function(e) {
    var that = this
    console.log(e.detail.errMsg)
    console.log(e.detail.userInfo)
    console.log(e.detail.rawData)

    const sysInfo = wx.getSystemInfoSync()
    wx.login({
        success: function(res) {
          console.log('doLogin success:' + res)
          /**
           * https://developers.weixin.qq.com/miniprogram/dev/api/wx.getUserInfo.html
           * 获取登录的临时凭证
           * var code = res.code
           * var encryptedData = e.detail.encryptedData
           * var iv = e.detail.iv
           * 调用后端，获取微信的session_key, secret
           */
          var jsonData = {
            code: res.code,
            userInfo: e.detail.userInfo,
            encryptedData: e.detail.encryptedData,
            iv: e.detail.iv,
            signature: e.detail.signature,
            sysInfo: sysInfo
          }
          console.log(jsonData)

          wx.request({
            url: urlList.urlList.loginUrl,
            //data: jsonData,
            data: {
              code: res.code,
              encryptedData: e.detail.encryptedData,
              iv: e.detail.iv,
              sysInfo: JSON.stringify(sysInfo)
              //userInfo: e.detail.rawData,
              //signature: e.detail.signature
            },
            header: {
              /**
               * 'Content-Type': 'application/json'用在get请求中没问题.默认是application/json
               *  POST请求需要换成：'Content-Type': 'application/x-www-form-urlencoded'
               */
              'content-type': 'application/x-www-form-urlencoded'
            },
            method: "POST",
            success: result => {
              console.log(result.data);
              if (result.data.status == 200) {
                //wx.setStorageSync('loginSessionKey', result.data.data)
                wx.showToast({
                  title: '登录成功',
                  icon: 'success',
                  duration: 2000, //提示的延迟时间
                  mask: true, //显示透明蒙层，防止触摸穿透
                  // success(res) {
                  //   wx.navigateTo({
                  //     url: '../news/news',
                  //   })
                  // }
                })
                setTimeout(function() {
                  wx.navigateTo({
                    url: '../news/news'
                  })
                }, 2000)
                // 保存用户信息到本地缓存，可以用作小程序端的拦截器
                app.globalData = e.detail.userInfo;
              } else {
                console.error('服务端未响应', result.errMsg)
                wx.showToast({
                  title: '登录失败' + result.errMsg,
                  icon: 'none',
                  duration: 2000
                })
              }

            },
            fail: result => {
              console.error('登录超时:' + result.errMsg);
              wx.showToast({
                title: '登录超时:' + result.errMsg,
                icon: 'none',
                duration: 2000
              })
            }
          })
        },
        fail: (err) => {
          console.error('网络异常!' + err.errMsg)
          wx.showToast({
            title: '网络异常!' + err.errMsg,
            icon: 'none',
            duration: 2000
          })
        }
      }),
      // 打开调试
      wx.setEnableDebug({
        enableDebug: true
      })
  }
})