let basePath = 'https://fj.ngrok.xiaomiqiu.cn/api';
let urlList = {
  loginUrl: basePath + '/wxLogin', // 登录
  refreshTokenUrl: basePath + '/refreshToken', // 刷新token
  getBlogListUrl: basePath + '/blogList', // 博客列表
  showDetailUrl: basePath + '/showBlogDetail', // 博客详情
  add: basePath + '/add', // 添加博客
  update: basePath + '/update', // 修改博客
  del: basePath + '/delete', // 删除博客
  fileUpload: basePath + '/upload', // 上传文件
  wxUserList: basePath + '/wxUserList', //微信小程序用户列表
  suggestionSubmit: basePath + '/suggestionsAdd' //意见反馈提交
}
module.exports = {
  urlList: urlList
}