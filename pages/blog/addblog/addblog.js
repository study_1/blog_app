// pages/blog/addblog/addblog.js
const urlList = require('../../resource/js/config.js');
Page({

  /**
   * 页面的初始数据
   */
  data: {
    image: '',
    loading: false, //按钮是否显示加载效果
    buttonName: '提交', // 提交按钮
    submitFlag: false // 防重提交
  },
  formSubmit(e) {
    console.log('form触发submit事件，携带数据为：', e.detail.value)
    let that = this
    let title = e.detail.value.title
    let content = e.detail.value.content
    let tip = e.detail.value.tip
    let link = e.detail.value.link
    let remark = e.detail.value.remark
    if (title == 0 || title == null) {
      wx.showToast({
        title: '请填写标题!',
        icon: 'none',
        duration: 1500
      })
      return
    } else if (tip == 0 || tip == null) {
      wx.showToast({
        title: '请填写摘要!',
        icon: 'none',
        duration: 1500
      })
      return
    } else if (content == 0 || content == null) {
      wx.showToast({
        title: '请填写内容!',
        icon: 'none',
        duration: 1500
      })
      return
    } else if (link == 0 || link == null) {
      wx.showToast({
        title: '请填写链接!',
        icon: 'none',
        duration: 1500
      })
      return
    } else if (remark == 0 || remark == null) {
      wx.showToast({
        title: '请填写备注信息!',
        icon: 'none',
        duration: 1500
      })
      return
    }
    // 构建json数据
    var jsonData = {
      data: e.detail.value
    }
    console.log(jsonData)
    that.setData({
      submitFlag: true,
      loading: true,
      buttonName: '正在提交'
    })
    wx.request({
      header: {
        'content-type': 'application/x-www-form-urlencoded'
      },
      method: 'POST',
      url: urlList.urlList.add,
      dataType: 'JSON',
      //data: jsonData
      data: {
        title: title,
        tip: tip,
        content: content,
        link: link,
        remark: remark,
        type: e.detail.value.type,
        label: e.detail.value.label,
        image: e.detail.value.image
      },
      success: res => {
        console.log('请求结果res:' + res.data)
        let data = JSON.parse(res.data)
        console.log('返回code:' + data.status + '返回信息:' + data.msg)
        if (data.status == 200) {
          wx.showToast({
            title: '提交成功',
            icon: 'none',
            duration: 2000,
            mask: true,
            success: function() {
              /**
               * 提交成功，返回博客列表页，并刷新博客列表页(需要在blog.js的onShow()方法中实现数据刷新)
               * onLoad()方法只加载一次，存放不变的数据
               * onShow()方法前后台切换都加载
               */
              setTimeout(function() {
                wx.navigateBack({
                  url: '../blog/blog'
                })
              }, 2000)

            }
          })
        } else {
          console.error('提交失败:', res)
          console.error('提交失败,code:' + data.status + '错误信息:' + data.msg)
          wx.showToast({
            title: '提交失败,code:' + data.status + '错误信息:' + data.msg,
            icon: 'none',
            duration: 2000
          })
        }
      },
      fail: (err) => {
        console.error('网络异常:', err)
        wx.showToast({
          title: '网络异常,' + err.errMsg,
          icon: 'none',
          duration: 2000
        })
      }
    })
  },
  chooseImage(e) {
    var that_ = this
    wx.chooseImage({
      sizeType: ['original', 'compressed'], //可选择原图或压缩后的图片
      sourceType: ['album', 'camera'], //可选择性开放访问相册、相机
      success: function(res) {
        that_.setData({
          image: that_.data.image.concat(res.tempFilePaths)
        })
      }
    })
  },
  previewImage: function(e) {
    wx.previewImage({
      current: e.currentTarget.id, // 当前显示图片的http链接
      urls: this.data.image // 需要预览的图片http链接列表
    })
  },

  formReset() {
    console.log('form触发reset事件')
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function(options) {

  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function() {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function() {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function() {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function() {

  }
})