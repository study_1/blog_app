const util = require('../../../utils/util.js')
Page({

  /**
   * 页面的初始数据
   */
  data: {
    title: null,
    content: null,
    tip: null,
    label: null,
    link: null,
    type: null,
    creator: null,
    createTime: null,
    pic: null,
    remark: null,
    viewCount: null,
    errMsg: '图片加载异常',
    errImg: ''
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function(options) {
    console.log(options)
    var that = this;
    let data = wx.getStorageSync("blog")
    console.log('获取到缓存数据:', data)
    let json = JSON.parse(data)
    let pic = json.pic == undefined ? errImg : json.pic
    console.log('PIC:' + pic)
    that.setData({
      title: json.title,
      content: json.content,
      tip: json.tip,
      label: json.label,
      link: json.link,
      type: typeInfo(json.type),
      creator: json.creator,
      createTime: timestampToTime(json.createTime),
      pic: pic,
      remark: json.remark,
      viewCount: json.viewCount
    })

    function typeInfo(value) {
      switch (value) {
        case '0':
          return '默认笔记'
        case '1':
          return '备忘录'
        case '2':
          return '知识库'
        default:
          return '未知'
      }
    }

    function timestampToTime(timestamp) {
      var date = new Date(timestamp); //时间戳为10位需*1000，时间戳为13位的话不需乘1000
      var Y = date.getFullYear() + '-';
      var M = (date.getMonth() + 1 < 10 ? '0' + (date.getMonth() + 1) : date.getMonth() + 1) + '-';
      var D = date.getDate() + ' ';
      var h = date.getHours() + ':';
      var m = date.getMinutes() + ':';
      var s = date.getSeconds();
      return Y + M + D + h + m + s;
    }
  },
  // 缺省图
  errImg: function() {
    let img = '../../resource/images/pic_160.png'
    this.setData({
      errImg: img
    })
  },
  onLikeClick: function() {
    wx.showToast({
      title: '别点了，只是个装饰',
      icon: 'none',
      duration: 2000
    })
  },
  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function() {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function() {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function() {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function() {

  }
})