// pages/wxuser/wxuser.js
const urlList = require('../resource/js/config.js');
Page({

  /**
   * 页面的初始数据
   */
  data: {
    items: []
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function(options) {
    var that = this;
    wx.showToast({
      title: '加载中',
      icon: 'loading',
      duration: 2000
    })
    let data = {
      page: that.data.page
    }
    wx.request({
      url: urlList.urlList.wxUserList,
      method: "GET",
      header: {
        'content-type': 'application/json'
      },
      dataType: 'json',
      data: data,
      success: result => {
        console.log('请求结果:' + JSON.stringify(result.data));
        if (result.data.status == 200) {
          wx.hideToast(); //请求成功，隐藏提示框
          console.info('items:' + JSON.stringify(result.data.data.blog)); //res.data相当于ajax里面的data,为后台返回的数据
          that.setData({
            items: result.data.data.blog
          })
        } else {
          console.error('获取数据失败,code:' + result.data.status + '错误信息:' + result.data.msg)
          wx.showToast({
            title: '获取数据失败,code:' + result.data.status + '错误信息:' + result.data.msg,
            icon: 'none',
            duration: 2000
          })
        }

      },
      fail: (result) => {
        console.error('网络异常:', result.errMsg)
        wx.showToast({
          title: '网络异常,' + result.errMsg,
          icon: 'none',
          duration: 2000
        })
      }
    })

  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function() {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function() {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function() {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function() {

  }
})