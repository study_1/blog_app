// pages/suggestion/suggestion.js
const urlList = require('../resource/js/config.js');
Page({
  /**
   * 页面的初始数据
   */
  data: {
    loading: false, //按钮是否显示加载效果
    buttonName: '提交', // 提交按钮
    submitFlag: false // 防重提交
  },
  formSubmit(e) {
    console.log('form触发submit事件，携带数据为：', e.detail.value)
    let that = this
    let content = e.detail.value.content
    if (content == 0 || content == null) {
      wx.showToast({
        title: '请填写意见反馈内容!',
        icon: 'none',
        duration: 1500
      })
      return
    }
    let sysInfoData = JSON.parse(JSON.stringify(wx.getStorageSync("sysInfo")))
    let userInfoData = JSON.parse(JSON.stringify(wx.getStorageSync("userInfo")))
    // 构建json数据
    var jsonData = {
      data: e.detail.value
    }
    that.setData({
      submitFlag: true,
      loading: true,
      buttonName: '正在提交'
    })
    wx.request({
      header: {
        'content-type': 'application/x-www-form-urlencoded'
      },
      method: 'POST',
      url: urlList.urlList.suggestionSubmit,
      dataType: 'JSON',
      //data: jsonData
      data: {
        content: content,
        nickName: userInfoData.nickName,
        avatarUrl: userInfoData.avatarUrl,
        model: sysInfoData.model,
        system: sysInfoData.system
      },
      success: res => {
        console.log('请求结果res:' + res.data)
        let data = JSON.parse(res.data)
        console.log('返回code:' + data.status + '返回信息:' + data.msg)
        if (data.status == 200) {
          wx.showToast({
            title: '提交成功',
            icon: 'none',
            duration: 2000,
            mask: true,
            success: function() {
              /**
               * 提交成功，返回博客列表页，并刷新博客列表页(需要在blog.js的onShow()方法中实现数据刷新)
               * onLoad()方法只加载一次，存放不变的数据
               * onShow()方法前后台切换都加载
               */
              setTimeout(function() {
                wx.navigateBack({
                  url: '../index/index'
                })
              }, 2000)

            }
          })
        } else {
          console.error('提交失败:', res)
          console.error('提交失败,code:' + data.status + '错误信息:' + data.msg)
          wx.showToast({
            title: '提交失败,code:' + data.status + '错误信息:' + data.msg,
            icon: 'none',
            duration: 2000
          })
        }
      },
      fail: (err) => {
        console.error('网络异常:', err)
        wx.showToast({
          title: '网络异常,' + err.errMsg,
          icon: 'none',
          duration: 2000
        })
      }
    })
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function(options) {

  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function() {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function() {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function() {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function() {

  }
})