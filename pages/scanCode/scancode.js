let app = getApp();
Page({
  data: {
    img: "../resource/images/r.jpg"
  },
  onLoad() {},
  scan() {
    var that = this
    wx.scanCode({
      success: (res) => {
        console.log("扫码结果:", res);
        wx.showToast({
          title: '扫码成功',
          icon: 'none',
          duration: 2000
        })
        that.setData({
          img: res.result
        })
      },
      fail: (res) => {
        console.error(res.errMsg)
        if (res.errMsg == 'scanCode:fail cancel') {
          wx.showToast({
            title: '取消扫码',
            icon: 'none',
            duration: 2000
          })
        } else {
          wx.showToast({
            title: '扫码异常',
            icon: 'none',
            duration: 2000
          })
        }
      },
    })
  }
})